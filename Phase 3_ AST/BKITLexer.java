// Generated from BKIT.g4 by ANTLR 4.9.2



import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BKITLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMMENT=1, ID=2, BODY=3, BREAK=4, CONT=5, DO=6, ELSE=7, ELSEIF=8, ENDBODY=9, 
		ENDIF=10, ENDFOR=11, ENDWHILE=12, FOR=13, FUNC=14, IF=15, PARA=16, RETURN=17, 
		THEN=18, VAR=19, WHILE=20, ENDDO=21, ADD=22, ADDF=23, SUB=24, SUBF=25, 
		MUL=26, MULF=27, DIV=28, DIVF=29, MOD=30, ASGN=31, NOT=32, AND=33, OR=34, 
		EQUAL=35, NEQUAL=36, LESS=37, GREATER=38, SEQUAL=39, LEQUAL=40, NEQUALF=41, 
		LESSF=42, GREATERF=43, SEQUALF=44, LEQUALF=45, RBRACKET_L=46, RBRACKET_R=47, 
		SBRACKET_L=48, SBRACKET_R=49, SEMI=50, DOT=51, COMMA=52, COLON=53, CBRACKET_L=54, 
		CBRACKET_R=55, INT_LIT=56, FLOAT_LIT=57, BOOL_LIT=58, STRING_LIT=59, WS=60, 
		UNCLOSE_STRING=61, ILLEGAL_ESCAPE=62, UNTERMINATE_COMMENT=63, ARRAY_LIT=64, 
		ERROR_CHAR=65;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"COMMENT", "ID", "BODY", "BREAK", "CONT", "DO", "ELSE", "ELSEIF", "ENDBODY", 
			"ENDIF", "ENDFOR", "ENDWHILE", "FOR", "FUNC", "IF", "PARA", "RETURN", 
			"THEN", "VAR", "WHILE", "TRUE", "FALSE", "ENDDO", "ADD", "ADDF", "SUB", 
			"SUBF", "MUL", "MULF", "DIV", "DIVF", "MOD", "ASGN", "NOT", "AND", "OR", 
			"EQUAL", "NEQUAL", "LESS", "GREATER", "SEQUAL", "LEQUAL", "NEQUALF", 
			"LESSF", "GREATERF", "SEQUALF", "LEQUALF", "RBRACKET_L", "RBRACKET_R", 
			"SBRACKET_L", "SBRACKET_R", "SEMI", "DOT", "COMMA", "COLON", "CBRACKET_L", 
			"CBRACKET_R", "DEC_LIT", "HEX_LIT", "OCT_LIT", "INT_LIT", "DecimalPart", 
			"Digits", "ExponentPart", "FLOAT_LIT", "BOOL_LIT", "LegalEscape", "StringElement", 
			"STRING_LIT", "WS", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "UNTERMINATE_COMMENT", 
			"ARRAY_LIT", "ERROR_CHAR"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "'Body'", "'Break'", "'Continue'", "'Do'", "'Else'", 
			"'ElseIf'", "'EndBody'", "'EndIf'", "'EndFor'", "'EndWhile'", "'For'", 
			"'Function'", "'If'", "'Parameter'", "'Return'", "'Then'", "'Var'", "'While'", 
			"'EndDo'", "'+'", "'+.'", "'-'", "'-.'", "'*'", "'*.'", "'\\'", "'\\.'", 
			"'%'", "'='", "'!'", "'&&'", "'||'", "'=='", "'!='", "'<'", "'>'", "'<='", 
			"'>='", "'=/='", "'<.'", "'>.'", "'<=.'", "'>=.'", "'('", "')'", "'['", 
			"']'", "';'", "'.'", "','", "':'", "'{'", "'}'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMMENT", "ID", "BODY", "BREAK", "CONT", "DO", "ELSE", "ELSEIF", 
			"ENDBODY", "ENDIF", "ENDFOR", "ENDWHILE", "FOR", "FUNC", "IF", "PARA", 
			"RETURN", "THEN", "VAR", "WHILE", "ENDDO", "ADD", "ADDF", "SUB", "SUBF", 
			"MUL", "MULF", "DIV", "DIVF", "MOD", "ASGN", "NOT", "AND", "OR", "EQUAL", 
			"NEQUAL", "LESS", "GREATER", "SEQUAL", "LEQUAL", "NEQUALF", "LESSF", 
			"GREATERF", "SEQUALF", "LEQUALF", "RBRACKET_L", "RBRACKET_R", "SBRACKET_L", 
			"SBRACKET_R", "SEMI", "DOT", "COMMA", "COLON", "CBRACKET_L", "CBRACKET_R", 
			"INT_LIT", "FLOAT_LIT", "BOOL_LIT", "STRING_LIT", "WS", "UNCLOSE_STRING", 
			"ILLEGAL_ESCAPE", "UNTERMINATE_COMMENT", "ARRAY_LIT", "ERROR_CHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	@Override
	public Token emit() {
	    Token result = super.emit();
	    switch (getType()) {
	    case UNCLOSE_STRING:
	        throw new UncloseString(result.getText());
	    case UNTERMINATE_COMMENT:
	        throw new UnterminatedComment(result.getText());
	    case ILLEGAL_ESCAPE:
	    	throw new IllegalEscape(result.getText());
	    case ERROR_CHAR:
	    	throw new ErrorToken(result.getText());
	    default:
	        return super.emit();
	    }
	}


	public BKITLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BKIT.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 68:
			STRING_LIT_action((RuleContext)_localctx, actionIndex);
			break;
		case 70:
			UNCLOSE_STRING_action((RuleContext)_localctx, actionIndex);
			break;
		case 71:
			ILLEGAL_ESCAPE_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void STRING_LIT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			setText(getText().substring(1, getText().length()-1));
			break;
		}
	}
	private void UNCLOSE_STRING_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			setText(getText().substring(1, getText().length()-1));
			break;
		}
	}
	private void ILLEGAL_ESCAPE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2:
			setText(getText().substring(1, getText().length()-1));
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2C\u021b\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\3\2\3\2\3\2\3\2\7\2\u009e\n\2\f\2\16\2\u00a1\13"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\7\3\u00aa\n\3\f\3\16\3\u00ad\13\3\3\4\3"+
		"\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\34\3\34"+
		"\3\34\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3#\3#"+
		"\3$\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3(\3(\3)\3)\3*\3*\3*\3+\3+\3+"+
		"\3,\3,\3,\3,\3-\3-\3-\3.\3.\3.\3/\3/\3/\3/\3\60\3\60\3\60\3\60\3\61\3"+
		"\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\67\3\67\38\38"+
		"\39\39\3:\3:\3;\3;\3;\7;\u018a\n;\f;\16;\u018d\13;\5;\u018f\n;\3<\3<\3"+
		"<\3<\7<\u0195\n<\f<\16<\u0198\13<\3=\3=\3=\3=\7=\u019e\n=\f=\16=\u01a1"+
		"\13=\3>\3>\3>\5>\u01a6\n>\3?\3?\7?\u01aa\n?\f?\16?\u01ad\13?\3@\6@\u01b0"+
		"\n@\r@\16@\u01b1\3A\3A\5A\u01b6\nA\3A\3A\3B\3B\3B\5B\u01bd\nB\3B\5B\u01c0"+
		"\nB\3C\3C\5C\u01c4\nC\3D\3D\3E\3E\3E\3E\3E\5E\u01cd\nE\3F\3F\7F\u01d1"+
		"\nF\fF\16F\u01d4\13F\3F\3F\3F\3G\6G\u01da\nG\rG\16G\u01db\3G\3G\3H\3H"+
		"\7H\u01e2\nH\fH\16H\u01e5\13H\3H\5H\u01e8\nH\3H\3H\3I\3I\7I\u01ee\nI\f"+
		"I\16I\u01f1\13I\3I\3I\3I\3I\5I\u01f7\nI\3I\3I\3J\3J\3J\3J\3J\3J\7J\u0201"+
		"\nJ\fJ\16J\u0204\13J\3K\3K\3K\3K\3K\3K\3K\3K\3K\3K\3K\7K\u0211\nK\fK\16"+
		"K\u0214\13K\5K\u0216\nK\3K\3K\3L\3L\3\u009f\2M\3\3\5\4\7\5\t\6\13\7\r"+
		"\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25"+
		")\26+\2-\2/\27\61\30\63\31\65\32\67\339\34;\35=\36?\37A C!E\"G#I$K%M&"+
		"O\'Q(S)U*W+Y,[-]._/a\60c\61e\62g\63i\64k\65m\66o\67q8s9u\2w\2y\2{:}\2"+
		"\177\2\u0081\2\u0083;\u0085<\u0087\2\u0089\2\u008b=\u008d>\u008f?\u0091"+
		"@\u0093A\u0095B\u0097C\3\2\26\3\2c|\6\2\62;C\\aac|\3\2\63;\3\2\62;\4\2"+
		"ZZzz\4\2\63;CH\4\2\62;CH\4\2QQqq\3\2\639\3\2\629\4\2GGgg\4\2--//\t\2)"+
		")^^ddhhppttvv\3\2^^\7\2\f\f\17\17$$))^^\5\2\13\f\17\17\"\"\4\3\f\f\17"+
		"\17\3\2))\3\2$$\3\2,,\2\u022f\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2"+
		"\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2"+
		"/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2"+
		"\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2"+
		"G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3"+
		"\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2"+
		"\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2"+
		"m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2{\3\2\2\2\2\u0083\3\2\2\2"+
		"\2\u0085\3\2\2\2\2\u008b\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091"+
		"\3\2\2\2\2\u0093\3\2\2\2\2\u0095\3\2\2\2\2\u0097\3\2\2\2\3\u0099\3\2\2"+
		"\2\5\u00a7\3\2\2\2\7\u00ae\3\2\2\2\t\u00b3\3\2\2\2\13\u00b9\3\2\2\2\r"+
		"\u00c2\3\2\2\2\17\u00c5\3\2\2\2\21\u00ca\3\2\2\2\23\u00d1\3\2\2\2\25\u00d9"+
		"\3\2\2\2\27\u00df\3\2\2\2\31\u00e6\3\2\2\2\33\u00ef\3\2\2\2\35\u00f3\3"+
		"\2\2\2\37\u00fc\3\2\2\2!\u00ff\3\2\2\2#\u0109\3\2\2\2%\u0110\3\2\2\2\'"+
		"\u0115\3\2\2\2)\u0119\3\2\2\2+\u011f\3\2\2\2-\u0124\3\2\2\2/\u012a\3\2"+
		"\2\2\61\u0130\3\2\2\2\63\u0132\3\2\2\2\65\u0135\3\2\2\2\67\u0137\3\2\2"+
		"\29\u013a\3\2\2\2;\u013c\3\2\2\2=\u013f\3\2\2\2?\u0141\3\2\2\2A\u0144"+
		"\3\2\2\2C\u0146\3\2\2\2E\u0148\3\2\2\2G\u014a\3\2\2\2I\u014d\3\2\2\2K"+
		"\u0150\3\2\2\2M\u0153\3\2\2\2O\u0156\3\2\2\2Q\u0158\3\2\2\2S\u015a\3\2"+
		"\2\2U\u015d\3\2\2\2W\u0160\3\2\2\2Y\u0164\3\2\2\2[\u0167\3\2\2\2]\u016a"+
		"\3\2\2\2_\u016e\3\2\2\2a\u0172\3\2\2\2c\u0174\3\2\2\2e\u0176\3\2\2\2g"+
		"\u0178\3\2\2\2i\u017a\3\2\2\2k\u017c\3\2\2\2m\u017e\3\2\2\2o\u0180\3\2"+
		"\2\2q\u0182\3\2\2\2s\u0184\3\2\2\2u\u018e\3\2\2\2w\u0190\3\2\2\2y\u0199"+
		"\3\2\2\2{\u01a5\3\2\2\2}\u01a7\3\2\2\2\177\u01af\3\2\2\2\u0081\u01b3\3"+
		"\2\2\2\u0083\u01b9\3\2\2\2\u0085\u01c3\3\2\2\2\u0087\u01c5\3\2\2\2\u0089"+
		"\u01cc\3\2\2\2\u008b\u01ce\3\2\2\2\u008d\u01d9\3\2\2\2\u008f\u01df\3\2"+
		"\2\2\u0091\u01eb\3\2\2\2\u0093\u01fa\3\2\2\2\u0095\u0205\3\2\2\2\u0097"+
		"\u0219\3\2\2\2\u0099\u009a\7,\2\2\u009a\u009b\7,\2\2\u009b\u009f\3\2\2"+
		"\2\u009c\u009e\13\2\2\2\u009d\u009c\3\2\2\2\u009e\u00a1\3\2\2\2\u009f"+
		"\u00a0\3\2\2\2\u009f\u009d\3\2\2\2\u00a0\u00a2\3\2\2\2\u00a1\u009f\3\2"+
		"\2\2\u00a2\u00a3\7,\2\2\u00a3\u00a4\7,\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6"+
		"\b\2\2\2\u00a6\4\3\2\2\2\u00a7\u00ab\t\2\2\2\u00a8\u00aa\t\3\2\2\u00a9"+
		"\u00a8\3\2\2\2\u00aa\u00ad\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00ac\3\2"+
		"\2\2\u00ac\6\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ae\u00af\7D\2\2\u00af\u00b0"+
		"\7q\2\2\u00b0\u00b1\7f\2\2\u00b1\u00b2\7{\2\2\u00b2\b\3\2\2\2\u00b3\u00b4"+
		"\7D\2\2\u00b4\u00b5\7t\2\2\u00b5\u00b6\7g\2\2\u00b6\u00b7\7c\2\2\u00b7"+
		"\u00b8\7m\2\2\u00b8\n\3\2\2\2\u00b9\u00ba\7E\2\2\u00ba\u00bb\7q\2\2\u00bb"+
		"\u00bc\7p\2\2\u00bc\u00bd\7v\2\2\u00bd\u00be\7k\2\2\u00be\u00bf\7p\2\2"+
		"\u00bf\u00c0\7w\2\2\u00c0\u00c1\7g\2\2\u00c1\f\3\2\2\2\u00c2\u00c3\7F"+
		"\2\2\u00c3\u00c4\7q\2\2\u00c4\16\3\2\2\2\u00c5\u00c6\7G\2\2\u00c6\u00c7"+
		"\7n\2\2\u00c7\u00c8\7u\2\2\u00c8\u00c9\7g\2\2\u00c9\20\3\2\2\2\u00ca\u00cb"+
		"\7G\2\2\u00cb\u00cc\7n\2\2\u00cc\u00cd\7u\2\2\u00cd\u00ce\7g\2\2\u00ce"+
		"\u00cf\7K\2\2\u00cf\u00d0\7h\2\2\u00d0\22\3\2\2\2\u00d1\u00d2\7G\2\2\u00d2"+
		"\u00d3\7p\2\2\u00d3\u00d4\7f\2\2\u00d4\u00d5\7D\2\2\u00d5\u00d6\7q\2\2"+
		"\u00d6\u00d7\7f\2\2\u00d7\u00d8\7{\2\2\u00d8\24\3\2\2\2\u00d9\u00da\7"+
		"G\2\2\u00da\u00db\7p\2\2\u00db\u00dc\7f\2\2\u00dc\u00dd\7K\2\2\u00dd\u00de"+
		"\7h\2\2\u00de\26\3\2\2\2\u00df\u00e0\7G\2\2\u00e0\u00e1\7p\2\2\u00e1\u00e2"+
		"\7f\2\2\u00e2\u00e3\7H\2\2\u00e3\u00e4\7q\2\2\u00e4\u00e5\7t\2\2\u00e5"+
		"\30\3\2\2\2\u00e6\u00e7\7G\2\2\u00e7\u00e8\7p\2\2\u00e8\u00e9\7f\2\2\u00e9"+
		"\u00ea\7Y\2\2\u00ea\u00eb\7j\2\2\u00eb\u00ec\7k\2\2\u00ec\u00ed\7n\2\2"+
		"\u00ed\u00ee\7g\2\2\u00ee\32\3\2\2\2\u00ef\u00f0\7H\2\2\u00f0\u00f1\7"+
		"q\2\2\u00f1\u00f2\7t\2\2\u00f2\34\3\2\2\2\u00f3\u00f4\7H\2\2\u00f4\u00f5"+
		"\7w\2\2\u00f5\u00f6\7p\2\2\u00f6\u00f7\7e\2\2\u00f7\u00f8\7v\2\2\u00f8"+
		"\u00f9\7k\2\2\u00f9\u00fa\7q\2\2\u00fa\u00fb\7p\2\2\u00fb\36\3\2\2\2\u00fc"+
		"\u00fd\7K\2\2\u00fd\u00fe\7h\2\2\u00fe \3\2\2\2\u00ff\u0100\7R\2\2\u0100"+
		"\u0101\7c\2\2\u0101\u0102\7t\2\2\u0102\u0103\7c\2\2\u0103\u0104\7o\2\2"+
		"\u0104\u0105\7g\2\2\u0105\u0106\7v\2\2\u0106\u0107\7g\2\2\u0107\u0108"+
		"\7t\2\2\u0108\"\3\2\2\2\u0109\u010a\7T\2\2\u010a\u010b\7g\2\2\u010b\u010c"+
		"\7v\2\2\u010c\u010d\7w\2\2\u010d\u010e\7t\2\2\u010e\u010f\7p\2\2\u010f"+
		"$\3\2\2\2\u0110\u0111\7V\2\2\u0111\u0112\7j\2\2\u0112\u0113\7g\2\2\u0113"+
		"\u0114\7p\2\2\u0114&\3\2\2\2\u0115\u0116\7X\2\2\u0116\u0117\7c\2\2\u0117"+
		"\u0118\7t\2\2\u0118(\3\2\2\2\u0119\u011a\7Y\2\2\u011a\u011b\7j\2\2\u011b"+
		"\u011c\7k\2\2\u011c\u011d\7n\2\2\u011d\u011e\7g\2\2\u011e*\3\2\2\2\u011f"+
		"\u0120\7V\2\2\u0120\u0121\7t\2\2\u0121\u0122\7w\2\2\u0122\u0123\7g\2\2"+
		"\u0123,\3\2\2\2\u0124\u0125\7H\2\2\u0125\u0126\7c\2\2\u0126\u0127\7n\2"+
		"\2\u0127\u0128\7u\2\2\u0128\u0129\7g\2\2\u0129.\3\2\2\2\u012a\u012b\7"+
		"G\2\2\u012b\u012c\7p\2\2\u012c\u012d\7f\2\2\u012d\u012e\7F\2\2\u012e\u012f"+
		"\7q\2\2\u012f\60\3\2\2\2\u0130\u0131\7-\2\2\u0131\62\3\2\2\2\u0132\u0133"+
		"\7-\2\2\u0133\u0134\7\60\2\2\u0134\64\3\2\2\2\u0135\u0136\7/\2\2\u0136"+
		"\66\3\2\2\2\u0137\u0138\7/\2\2\u0138\u0139\7\60\2\2\u01398\3\2\2\2\u013a"+
		"\u013b\7,\2\2\u013b:\3\2\2\2\u013c\u013d\7,\2\2\u013d\u013e\7\60\2\2\u013e"+
		"<\3\2\2\2\u013f\u0140\7^\2\2\u0140>\3\2\2\2\u0141\u0142\7^\2\2\u0142\u0143"+
		"\7\60\2\2\u0143@\3\2\2\2\u0144\u0145\7\'\2\2\u0145B\3\2\2\2\u0146\u0147"+
		"\7?\2\2\u0147D\3\2\2\2\u0148\u0149\7#\2\2\u0149F\3\2\2\2\u014a\u014b\7"+
		"(\2\2\u014b\u014c\7(\2\2\u014cH\3\2\2\2\u014d\u014e\7~\2\2\u014e\u014f"+
		"\7~\2\2\u014fJ\3\2\2\2\u0150\u0151\7?\2\2\u0151\u0152\7?\2\2\u0152L\3"+
		"\2\2\2\u0153\u0154\7#\2\2\u0154\u0155\7?\2\2\u0155N\3\2\2\2\u0156\u0157"+
		"\7>\2\2\u0157P\3\2\2\2\u0158\u0159\7@\2\2\u0159R\3\2\2\2\u015a\u015b\7"+
		">\2\2\u015b\u015c\7?\2\2\u015cT\3\2\2\2\u015d\u015e\7@\2\2\u015e\u015f"+
		"\7?\2\2\u015fV\3\2\2\2\u0160\u0161\7?\2\2\u0161\u0162\7\61\2\2\u0162\u0163"+
		"\7?\2\2\u0163X\3\2\2\2\u0164\u0165\7>\2\2\u0165\u0166\7\60\2\2\u0166Z"+
		"\3\2\2\2\u0167\u0168\7@\2\2\u0168\u0169\7\60\2\2\u0169\\\3\2\2\2\u016a"+
		"\u016b\7>\2\2\u016b\u016c\7?\2\2\u016c\u016d\7\60\2\2\u016d^\3\2\2\2\u016e"+
		"\u016f\7@\2\2\u016f\u0170\7?\2\2\u0170\u0171\7\60\2\2\u0171`\3\2\2\2\u0172"+
		"\u0173\7*\2\2\u0173b\3\2\2\2\u0174\u0175\7+\2\2\u0175d\3\2\2\2\u0176\u0177"+
		"\7]\2\2\u0177f\3\2\2\2\u0178\u0179\7_\2\2\u0179h\3\2\2\2\u017a\u017b\7"+
		"=\2\2\u017bj\3\2\2\2\u017c\u017d\7\60\2\2\u017dl\3\2\2\2\u017e\u017f\7"+
		".\2\2\u017fn\3\2\2\2\u0180\u0181\7<\2\2\u0181p\3\2\2\2\u0182\u0183\7}"+
		"\2\2\u0183r\3\2\2\2\u0184\u0185\7\177\2\2\u0185t\3\2\2\2\u0186\u018f\7"+
		"\62\2\2\u0187\u018b\t\4\2\2\u0188\u018a\t\5\2\2\u0189\u0188\3\2\2\2\u018a"+
		"\u018d\3\2\2\2\u018b\u0189\3\2\2\2\u018b\u018c\3\2\2\2\u018c\u018f\3\2"+
		"\2\2\u018d\u018b\3\2\2\2\u018e\u0186\3\2\2\2\u018e\u0187\3\2\2\2\u018f"+
		"v\3\2\2\2\u0190\u0191\7\62\2\2\u0191\u0192\t\6\2\2\u0192\u0196\t\7\2\2"+
		"\u0193\u0195\t\b\2\2\u0194\u0193\3\2\2\2\u0195\u0198\3\2\2\2\u0196\u0194"+
		"\3\2\2\2\u0196\u0197\3\2\2\2\u0197x\3\2\2\2\u0198\u0196\3\2\2\2\u0199"+
		"\u019a\7\62\2\2\u019a\u019b\t\t\2\2\u019b\u019f\t\n\2\2\u019c\u019e\t"+
		"\13\2\2\u019d\u019c\3\2\2\2\u019e\u01a1\3\2\2\2\u019f\u019d\3\2\2\2\u019f"+
		"\u01a0\3\2\2\2\u01a0z\3\2\2\2\u01a1\u019f\3\2\2\2\u01a2\u01a6\5u;\2\u01a3"+
		"\u01a6\5w<\2\u01a4\u01a6\5y=\2\u01a5\u01a2\3\2\2\2\u01a5\u01a3\3\2\2\2"+
		"\u01a5\u01a4\3\2\2\2\u01a6|\3\2\2\2\u01a7\u01ab\7\60\2\2\u01a8\u01aa\t"+
		"\5\2\2\u01a9\u01a8\3\2\2\2\u01aa\u01ad\3\2\2\2\u01ab\u01a9\3\2\2\2\u01ab"+
		"\u01ac\3\2\2\2\u01ac~\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ae\u01b0\t\5\2\2"+
		"\u01af\u01ae\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1\u01af\3\2\2\2\u01b1\u01b2"+
		"\3\2\2\2\u01b2\u0080\3\2\2\2\u01b3\u01b5\t\f\2\2\u01b4\u01b6\t\r\2\2\u01b5"+
		"\u01b4\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b7\u01b8\5\177"+
		"@\2\u01b8\u0082\3\2\2\2\u01b9\u01bf\5\177@\2\u01ba\u01bc\5}?\2\u01bb\u01bd"+
		"\5\u0081A\2\u01bc\u01bb\3\2\2\2\u01bc\u01bd\3\2\2\2\u01bd\u01c0\3\2\2"+
		"\2\u01be\u01c0\5\u0081A\2\u01bf\u01ba\3\2\2\2\u01bf\u01be\3\2\2\2\u01c0"+
		"\u0084\3\2\2\2\u01c1\u01c4\5+\26\2\u01c2\u01c4\5-\27\2\u01c3\u01c1\3\2"+
		"\2\2\u01c3\u01c2\3\2\2\2\u01c4\u0086\3\2\2\2\u01c5\u01c6\t\16\2\2\u01c6"+
		"\u0088\3\2\2\2\u01c7\u01c8\7)\2\2\u01c8\u01cd\7$\2\2\u01c9\u01ca\t\17"+
		"\2\2\u01ca\u01cd\5\u0087D\2\u01cb\u01cd\n\20\2\2\u01cc\u01c7\3\2\2\2\u01cc"+
		"\u01c9\3\2\2\2\u01cc\u01cb\3\2\2\2\u01cd\u008a\3\2\2\2\u01ce\u01d2\7$"+
		"\2\2\u01cf\u01d1\5\u0089E\2\u01d0\u01cf\3\2\2\2\u01d1\u01d4\3\2\2\2\u01d2"+
		"\u01d0\3\2\2\2\u01d2\u01d3\3\2\2\2\u01d3\u01d5\3\2\2\2\u01d4\u01d2\3\2"+
		"\2\2\u01d5\u01d6\7$\2\2\u01d6\u01d7\bF\3\2\u01d7\u008c\3\2\2\2\u01d8\u01da"+
		"\t\21\2\2\u01d9\u01d8\3\2\2\2\u01da\u01db\3\2\2\2\u01db\u01d9\3\2\2\2"+
		"\u01db\u01dc\3\2\2\2\u01dc\u01dd\3\2\2\2\u01dd\u01de\bG\2\2\u01de\u008e"+
		"\3\2\2\2\u01df\u01e3\7$\2\2\u01e0\u01e2\5\u0089E\2\u01e1\u01e0\3\2\2\2"+
		"\u01e2\u01e5\3\2\2\2\u01e3\u01e1\3\2\2\2\u01e3\u01e4\3\2\2\2\u01e4\u01e7"+
		"\3\2\2\2\u01e5\u01e3\3\2\2\2\u01e6\u01e8\t\22\2\2\u01e7\u01e6\3\2\2\2"+
		"\u01e8\u01e9\3\2\2\2\u01e9\u01ea\bH\4\2\u01ea\u0090\3\2\2\2\u01eb\u01ef"+
		"\7$\2\2\u01ec\u01ee\5\u0089E\2\u01ed\u01ec\3\2\2\2\u01ee\u01f1\3\2\2\2"+
		"\u01ef\u01ed\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01f6\3\2\2\2\u01f1\u01ef"+
		"\3\2\2\2\u01f2\u01f3\t\17\2\2\u01f3\u01f7\n\16\2\2\u01f4\u01f5\t\23\2"+
		"\2\u01f5\u01f7\n\24\2\2\u01f6\u01f2\3\2\2\2\u01f6\u01f4\3\2\2\2\u01f7"+
		"\u01f8\3\2\2\2\u01f8\u01f9\bI\5\2\u01f9\u0092\3\2\2\2\u01fa\u01fb\7,\2"+
		"\2\u01fb\u01fc\7,\2\2\u01fc\u0202\3\2\2\2\u01fd\u0201\n\25\2\2\u01fe\u01ff"+
		"\7,\2\2\u01ff\u0201\n\25\2\2\u0200\u01fd\3\2\2\2\u0200\u01fe\3\2\2\2\u0201"+
		"\u0204\3\2\2\2\u0202\u0200\3\2\2\2\u0202\u0203\3\2\2\2\u0203\u0094\3\2"+
		"\2\2\u0204\u0202\3\2\2\2\u0205\u0215\5q9\2\u0206\u0216\5{>\2\u0207\u0216"+
		"\5\u0083B\2\u0208\u0216\5\u0085C\2\u0209\u0212\5\u008bF\2\u020a\u020b"+
		"\5m\67\2\u020b\u020c\5{>\2\u020c\u0211\3\2\2\2\u020d\u0211\5\u0083B\2"+
		"\u020e\u0211\5\u0085C\2\u020f\u0211\5\u008bF\2\u0210\u020a\3\2\2\2\u0210"+
		"\u020d\3\2\2\2\u0210\u020e\3\2\2\2\u0210\u020f\3\2\2\2\u0211\u0214\3\2"+
		"\2\2\u0212\u0210\3\2\2\2\u0212\u0213\3\2\2\2\u0213\u0216\3\2\2\2\u0214"+
		"\u0212\3\2\2\2\u0215\u0206\3\2\2\2\u0215\u0207\3\2\2\2\u0215\u0208\3\2"+
		"\2\2\u0215\u0209\3\2\2\2\u0215\u0216\3\2\2\2\u0216\u0217\3\2\2\2\u0217"+
		"\u0218\5s:\2\u0218\u0096\3\2\2\2\u0219\u021a\13\2\2\2\u021a\u0098\3\2"+
		"\2\2\34\2\u009f\u00ab\u018b\u018e\u0196\u019f\u01a5\u01ab\u01b1\u01b5"+
		"\u01bc\u01bf\u01c3\u01cc\u01d2\u01db\u01e3\u01e7\u01ef\u01f6\u0200\u0202"+
		"\u0210\u0212\u0215\6\b\2\2\3F\2\3H\3\3I\4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}