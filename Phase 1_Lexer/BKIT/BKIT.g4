/**
* Defined a grammar called MC
 */

 grammar BKIT;
 options{
     language=Java;
    //  language=Python3;
 }

 @lexer::header{

}

 @parser::header{

 }

program:;
//3. Lexical Structure

//3.2  Block Comments
COMMENT: '**' .*? '**' -> skip ;
//3.3.1 Identifiers
ID: [a-z][a-zA-Z0-9_]* ;

//3.3.2 Keywords
BODY: 'Body' ;
BREAK: 'Break' ;
CONT: 'Continue' ;
DO: 'Do' ;
ELSE: 'Else' ;
ELSEIF: 'ElseIf' ;
ENDBODY: 'EndBody' ;
ENDIF: 'EndIf' ;
ENDFOR: 'EndFor' ;
ENDWHILE: 'EndWhile' ;
FOR: 'For' ;
FUNC: 'Function' ;
IF: 'If' ;
PARA: 'Parameter' ;
RETURN: 'Return' ;
THEN: 'Then' ;
VAR: 'Var' ;
WHILE: 'While' ;
fragment TRUE: 'True' ;
fragment FALSE: 'False' ;
ENDDO: 'EndDo' ;

//3.3.3 Operators
ADD: '+' ;
ADDF: '+.' ;
SUB: '-' ;
SUBF: '-.' ;
MUL: '*' ;
MULF: '*.' ;
DIV: '\\' ;
DIVF: '\\.' ;
MOD: '%' ;


NOT: '!' ;
AND: '&&' ;
OR: '||' ;

EQUAL: '==' ;
NEQUAL: '!=' ;
SMALLER: '<' ;
LARGER: '>' ;
SEQUAL: '<=' ;
LEQUAL: '>=' ;

NEQUALF: '=/=' ;
SMALLERF: '<.' ;
LARGERF: '>.' ;
SEQUALF: '<=.' ;
LEQUALF: '>=.' ;
ASGN: '=';
//3.3.4 Seperators
RBRACKET_L: '(' ;
RBRACKET_R: ')' ;
SBRACKET_L: '[' ;
SBRACKET_R: ']' ;
SEMI: ';' ;
DOT: '.' ;
COMMA: ',' ;
COLON: ':' ;
CBRACKET_L: '{' ;
CBRACKET_R: '}' ;

fragment DEC_LIT: '0' | [1-9][0-9]*;
fragment HEX_LIT: '0'[xX][1-9A-F][0-9A-F]* ;
fragment OCT_LIT: '0'[oO][1-7][0-7]* ;
INT_LIT: DEC_LIT | HEX_LIT | OCT_LIT;

fragment DecimalPart: ('.'[0-9]*) ;
fragment Digits: [0-9]+ ;
fragment ExponentPart: [eE] [+-]? Digits ;
FLOAT_LIT: Digits (DecimalPart ExponentPart? | ExponentPart) ;
BOOL_LIT:	(TRUE|FALSE) ;

fragment LegalEscape: [bfrnt'\\] ;
fragment StringElement:  '\'"' | [\\] LegalEscape | ~['\\\n\r"] ;
STRING_LIT:
	'"' StringElement* '"'
     {setText(getText().substring(1, getText().length()-1));};

//Whitespace
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines


//Errors Tokens
UNCLOSE_STRING:
	'"' StringElement* ('\r'|'\n'|EOF)
	     {setText(getText().substring(1, getText().length()-1));};

ILLEGAL_ESCAPE:
	'"'  StringElement* ( [\\]~[bfrnt'\\] | [']~["])
	     {setText(getText().substring(1, getText().length()-1));};

UNTERMINATED_COMMENT: '**' ( (~'*') | '*'(~'*') )*  ;



ARRAY_LIT: CBRACKET_L  (INT_LIT | OCT_LIT | HEX_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT (COMMA INT_LIT | OCT_LIT | HEX_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT)*)? CBRACKET_R  ;// the ? is for empty array


