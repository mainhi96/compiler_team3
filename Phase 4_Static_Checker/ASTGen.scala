import java.io.{File, PrintWriter}
import java.lang.RuntimeException

import org.antlr.v4.runtime.ANTLRFileStream
import org.antlr.v4.runtime.CommonTokenStream

trait AST
//case class Program(varDecl:List[VarDecl], funcDecl: List[FuncDecl]) extends AST
case class Program(decl: List[Decl]) extends AST
trait Stmt extends AST
case class AssignStmt (lhs: LHS, rhs: Exp) extends Stmt
case class IfStmt(ifList: List[(Exp,StmtList)], elseList: StmtList) extends Stmt
case class ForStmt(idx: Id, exp1: Exp, exp2: Exp, exp3: Exp,stmtList: StmtList ) extends Stmt
case class BreakStmt() extends Stmt
case class Continue() extends Stmt
case class ReturnStmt(exp: Exp) extends Stmt
case class DoWhileStmt(stmtList: StmtList,exp: Exp ) extends Stmt
case class WhileStmt(exp: Exp, stmtList: StmtList) extends Stmt
case class FuncCallStmt(method:Id, para: List[Exp]) extends Stmt
trait Exp extends AST
case class BinOp(op:String,e1:Exp,e2:Exp) extends Exp
case class UnOp(op:String,e1:Exp) extends Exp
case class FuncCallExp(name: Id, para: List[Exp]) extends Exp
trait Decl extends AST
case class VarDecl(varDecl: List[IdInit]) extends Decl
case class IdInit(variable: Id, varDimen: List[Int], varInit: Literal) extends Decl
case class FuncDecl(name: Id, param: FuncParam, body: FuncBody) extends Decl
case class FuncBody(stmtList: StmtList) extends Decl
case class FuncParam(param: List[Decl]) extends Decl
case class StmtList(varDecl: List[VarDecl], stmtList:List[Stmt]) extends Decl
trait Literal extends Exp
case class Intlit(lit:String) extends Literal// An integer literal may be in octal or hexadecimal
case class Floatlit(lit:Float) extends Literal
case class Stringlit(lit:String) extends Literal
case class Boollit(lit:Boolean) extends Literal
case class Arraylit(lit:List[Literal]) extends Literal
trait LHS extends Exp
case class Id(name: String) extends LHS
case class ArrayCell(arr: Exp, idx: List[Exp]) extends LHS //rename
case class IdVariable(id: String, dimen: List[Int]) extends LHS


class ASTGen extends BKITBaseVisitor[AST] {
  //program  : (var_declare SEMI)* func_declare* EOF ; //start symbol
  override def visitProgram(ctx: BKITParser.ProgramContext) = {
    println("Visit Prog")
    var res = List[Decl]()
    var len = ctx.var_declare().size()
    println("ABC " + len)
    var res2 = List[FuncDecl]()
    var len2 = ctx.func_declare().size()

    if (len > 0) {
      res = ctx.var_declare(0).accept(this).asInstanceOf[Decl] :: res
      for (i <- 1 to ctx.var_declare().size() - 1) {
        res = ctx.var_declare(i).accept(this).asInstanceOf[Decl] :: res
        println("VarDecl: " + res.length)
      }
    }
    if (len2 > 0) {
      println("ABC " + len2)
      res = ctx.func_declare(0).accept(this).asInstanceOf[Decl] :: res
      for (i <- 1 to ctx.func_declare().size() - 1) {
        res = ctx.func_declare(i).accept(this).asInstanceOf[Decl] :: res
        println("EXP: " + res.length)
      }

    }

    Program(res)

  }

  //  # all_literal: basic_literal | array_lit;
  override def visitAll_literal(ctx: BKITParser.All_literalContext) = {
    println("Visit All_literal")
    ctx.getChild(0).accept(this)
  }

  //single_lit: INT_LIT | FLOAT_LIT | BOOL_LIT |STRING_LIT;
  override def visitSingle_lit(ctx: BKITParser.Single_litContext) = {
    println("Visit Single_lit")
    if (ctx.INT_LIT() != null) {
      println("Visit INT_LIT: " + ctx.INT_LIT().getText)
      Intlit(ctx.INT_LIT().getText()) //An integer literal may be in octal or hexadecimal
    }
    else if (ctx.FLOAT_LIT() != null) {
      println("Visit FLOAT: " + ctx.FLOAT_LIT().getText)
      Floatlit(ctx.FLOAT_LIT().getText().toFloat)
    }
    else if (ctx.BOOL_LIT() != null) {
      println("Visit BOOL: " + ctx.BOOL_LIT().getText)
      Boollit(ctx.BOOL_LIT().getText().toBoolean)
    }
    else Stringlit(ctx.STRING_LIT().getText)
  }

  // array_lit: CBRACKET_L  (all_literal (COMMA all_literal)*)? CBRACKET_R  ;// the ? is for empty array
  override def visitArray_lit(ctx: BKITParser.Array_litContext) = {
    println("Visit Array_lit")
    var res = List[Literal]()
    if (ctx.all_literal().size() > 0)
      for (i <- 0 to ctx.all_literal().size() - 1) {
        res = ctx.all_literal(i).accept(this).asInstanceOf[Literal] :: res
      }
    Arraylit(res)
  }

  // func_declare: FUNC COLON ID func_para? func_body;
  override def visitFunc_declare(ctx: BKITParser.Func_declareContext) = {
    println("Visit Func_declare")

    var body = ctx.func_body().accept(this).asInstanceOf[FuncBody]

    if (ctx.func_para() != null) {
      var param = ctx.func_para().accept(this).asInstanceOf[FuncParam]
      FuncDecl(Id(ctx.ID().getText), param, body)
    }
    else

      FuncDecl(Id(ctx.ID().getText), null, body)
  }


  // func_body: BODY statement_list ENDBODY DOT;
  override def visitFunc_body(ctx: BKITParser.Func_bodyContext) = {
    println("Visit Func_body")
    FuncBody(ctx.statement_list().accept(this).asInstanceOf[StmtList])
  }

  //func_para: PARA COLON init_variable (COMMA init_variable)*;
  override def visitFunc_para(ctx: BKITParser.Func_paraContext) = {
    var res = List[Decl]()
    res = ctx.id_init(0).accept(this).asInstanceOf[Decl] :: res
    if (ctx.id_init().size() > 1)
      for (i <- 1 to ctx.id_init().size() - 1) {
        res = ctx.id_init(i).accept(this).asInstanceOf[Decl] :: res
      }
    FuncParam(res)

  }

  //statement_list: (var_declare SEMI)* statement*;
  override def visitStatement_list(ctx: BKITParser.Statement_listContext) = {
    println("Visit Statement_list")
    var varDeclList = List[VarDecl]()
    if (ctx.var_declare().size() > 0)
      for (i <- 0 to ctx.var_declare().size() - 1) {
        varDeclList = ctx.var_declare(i).accept(this).asInstanceOf[VarDecl] :: varDeclList
      }
    var stmtlList = List[Stmt]()
    if (ctx.statement().size() > 0) {
      println("check Statement_list")
      for (i <- 0 to ctx.statement().size() - 1) {
        stmtlList = ctx.statement(i).accept(this).asInstanceOf[Stmt] :: stmtlList
      }
    }
    StmtList(varDeclList, stmtlList)
  }

  // statement: (assignment | BREAK | CONT | funcCall | ret_stmt) SEMI
  // | (if_stmt | for_stmt | while_stmt | do_stmt);
  override def visitStatement(ctx: BKITParser.StatementContext) = {
    println("Visit Statement")
    if (ctx.BREAK() != null) BreakStmt()
    else if (ctx.CONT() != null) Continue()
   else {
     println(ctx.getChild(0).getText)
     ctx.getChild(0).accept(this)
   }

  }

  //var_declare: VAR COLON id_init (COMMA id_init)*;
  override def visitVar_declare(ctx: BKITParser.Var_declareContext) = {
    println("Visit Var declare")
    var varDeclList = List[IdInit]()
    varDeclList = ctx.id_init(0).accept(this).asInstanceOf[IdInit] :: varDeclList
    if (ctx.id_init().size() > 1)
      for (i <- 1 to ctx.id_init().size() - 1) {
        varDeclList = ctx.id_init(i).accept(this).asInstanceOf[IdInit] :: varDeclList
      }

    VarDecl(varDeclList)
  }

  //id_init: init_variable (ASGN all_literal)?;
  override def visitId_init(ctx: BKITParser.Id_initContext) = {
    println("Visit Id_init")
    var res = ctx.init_variable().accept(this).asInstanceOf[IdVariable]
    if (ctx.all_literal() != null) IdInit(Id(res.id), res.dimen, ctx.all_literal().accept(this).asInstanceOf[Literal])
    else IdInit(Id(res.id), res.dimen, null)
  }

  // init_variable: ID (SBRACKET_L (INT_LIT) SBRACKET_R)*;
  override def visitInit_variable(ctx: BKITParser.Init_variableContext) = {
    println("Visit Id_variable")
    var dimentions = List[Int]()
    var id = ctx.getChild(0).getText()
    if (ctx.SBRACKET_L().size() > 0) {
      for (i <- 0 to ctx.SBRACKET_L().size() - 1) {
        println(ctx.getChild(i * 3 + 2).getText())
        dimentions = ctx.getChild(i * 3 + 2).getText().toInt :: dimentions
      }

    }
    IdVariable(id, dimentions)
  }

  // assignment: (ID | index_exp) ASGN expression;
  override def visitAssignment(ctx: BKITParser.AssignmentContext) = {
    println("Visit Assignment")
    if (ctx.ID() != null) AssignStmt(Id(ctx.ID().getText), ctx.expression().accept(this).asInstanceOf[Exp])
    else AssignStmt(ctx.index_exp().accept(this).asInstanceOf[LHS], ctx.expression().accept(this).asInstanceOf[Exp])
  }

  //case class IfStmt(ifExp: Exp, ifStmt: List[StmtList])
  // if_stmt: IF expression THEN statement_list (ELSEIF expression THEN statement_list)* (ELSE statement_list)? ENDIF DOT;
  override def visitIf_stmt(ctx: BKITParser.If_stmtContext) = {
    println("Visit If_stmt")
    var ifList: List[(Exp, StmtList)] = List()
    var elseList = ctx.statement_list(ctx.statement_list.size() - 1).accept(this).asInstanceOf[StmtList]
    var sizeIf = ctx.THEN().size()
    var sizeElse = ctx.statement_list.size() - ctx.expression().size()
    ifList = List((ctx.expression(0).accept(this).asInstanceOf[Exp], ctx.statement_list(0).accept(this).asInstanceOf[StmtList]))
    if (sizeIf > 1)
      for (i <- 1 to sizeIf - 1) {
        ifList = (ctx.expression(i).accept(this).asInstanceOf[Exp], ctx.statement_list(i).accept(this).asInstanceOf[StmtList]) :: ifList
      }
    if (sizeElse == 0) {
      elseList = null
    }

    IfStmt(ifList, elseList)
  }

  //FOR RBRACKET_L ID ASGN expression COMMA expression COMMA expression RBRACKET_R DO statement_list ENDFOR DOT;
  override def visitFor_stmt(ctx: BKITParser.For_stmtContext) = {
    println("Visit For")
    ForStmt(Id(ctx.ID().getText()), ctx.expression(0).accept(this).asInstanceOf[Exp], ctx.expression(1).accept(this).asInstanceOf[Exp]
      , ctx.expression(2).accept(this).asInstanceOf[Exp], ctx.statement_list().accept(this).asInstanceOf[StmtList])

  }

  //while_stmt: WHILE expression DO statement_list ENDWHILE DOT;
  override def visitWhile_stmt(ctx: BKITParser.While_stmtContext) = {
    println("Visit While")
    WhileStmt(ctx.expression().accept(this).asInstanceOf[Exp], ctx.statement_list().accept(this).asInstanceOf[StmtList])

  }

  //dowhile_stmt: DO statement_list WHILE expression ENDDO DOT;
  override def visitDowhile_stmt(ctx: BKITParser.Dowhile_stmtContext) = {
    println("Visit Do While")
    DoWhileStmt( ctx.statement_list().accept(this).asInstanceOf[StmtList], ctx.expression().accept(this).asInstanceOf[Exp])

  }

  //  funcCall: ID RBRACKET_L (expression(COMMA expression)*)? RBRACKET_R;
  override def visitFuncCall(ctx: BKITParser.FuncCallContext) = {
    println("Visit FuncCall")
    var res = List[Exp]()
    if (ctx.expression().size() > 0)
      for (i <- 0 to ctx.expression().size() - 1) {
        res = ctx.expression(i).accept(this).asInstanceOf[Exp] :: res
      }
    FuncCallStmt(Id(ctx.ID().getText), res)


  }
  //ret_stmt: RETURN expression?;
  override def visitRet_stmt(ctx: BKITParser.Ret_stmtContext) = {
    println("Visit Return")

    if (ctx.expression() != null)
      ReturnStmt(ctx.expression().accept(this).asInstanceOf[Exp])
    else
      ReturnStmt(null)
  }

  //expression: exp2 (EQUAL | NEQUAL | LESS | GREATER | SEQUAL | LEQUAL | NEQUALF | LESSF | GREATERF | SEQUALF | LEQUALF) exp2
  //		  | exp2;
  override def visitExpression(ctx: BKITParser.ExpressionContext) = {
    println("Visit Expression")

    if (ctx.exp2().size() == 2)
      BinOp(ctx.getChild(1).getText(), ctx.exp2(0).accept(this).asInstanceOf[Exp], ctx.exp2(1).accept(this).asInstanceOf[Exp])
    else
      ctx.exp2(0).accept(this).asInstanceOf[Exp]

  }

  //    exp2: RBRACKET_L expression RBRACKET_R //not sure
  //	      | funcCall
  //	      | index_exp
  //	      | <assoc=right> (SUB|SUBF) exp2
  //	      | <assoc=right> (NOT) exp2
  //	      | exp2(MUL|MULF|DIV|DIVF|MOD)exp2
  //	      | exp2(ADD|ADDF|SUB|SUBF)exp2
  //	      | exp2(AND|OR)exp2
  //	      | ID
  //	      |  all_literal;
  //Can't resolve Return 3+2; => need to be enhance
  override def visitExp2(ctx: BKITParser.Exp2Context) = {
    println("Visit Exp2")


    if (ctx.RBRACKET_L() != null) ctx.expression().accept(this).asInstanceOf[Exp]
    else if (ctx.funcCall() != null) {
      var res = ctx.funcCall().accept(this).asInstanceOf[FuncCallStmt]
      FuncCallExp(res.method, res.para)
    }
   else if (ctx.index_exp() != null) ctx.index_exp().accept(this).asInstanceOf[ArrayCell]
    else if (ctx.ID()!=null)  Id(ctx.ID().getText())
    else if (ctx.all_literal() != null) ctx.all_literal().accept(this).asInstanceOf[Literal]


   else if (ctx.exp2().size == 1) UnOp(ctx.getChild(0).getText(), ctx.exp2(0).accept(this).asInstanceOf[Exp])



   else if (ctx.exp2().size == 2) BinOp(ctx.getChild(1).getText(), ctx.exp2(0).accept(this).asInstanceOf[Exp], ctx.exp2(1).accept(this).asInstanceOf[Exp])

   else if (ctx.getChild(0).accept(this).isInstanceOf[FuncCallStmt]) {
      var   res = ctx.getChild(0).accept(this).asInstanceOf[FuncCallStmt]
      FuncCallExp(res.method, res.para)
    }

    else {
      println("BHHH" + ctx.getChild(0).accept(this).asInstanceOf[ArrayCell])
      Id(ctx.getChild(0).toString)
    }

  }

  override def visitIndex_exp(ctx: BKITParser.Index_expContext) = {
    println("Visit Index_exp")
    var expList = List[Exp]()
    if (ctx.expression().size() > 0)
      for (i <- 0 to ctx.expression.size() - 1) {
        println(ctx.expression(i).getText)
        expList = ctx.expression(i).accept(this).asInstanceOf[Exp] :: expList
      }
    if (ctx.getChild(0).accept(this).isInstanceOf[FuncCallStmt]) {

      var res = ctx.funcCall().accept(this).asInstanceOf[FuncCallStmt]
      var funcCallExp = FuncCallExp(res.method, res.para)
      ArrayCell(funcCallExp, expList)

    }
    else ArrayCell(Id(ctx.ID().getText()), expList)
  }

}
class StaticCheck {
  var detailDecl: List[String] = List[String]()
  var indexDecl: List[String] = List[String]()

  def visit(ast: AST): String = {
    var res = "NONE"


    if (ast.isInstanceOf[Program]) {
      res = visitProgram(ast.asInstanceOf[Program])
    }
    else if (ast.isInstanceOf[BinOp]) {
      res = visitBinOp(ast.asInstanceOf[BinOp])
    }
    else if (ast.isInstanceOf[UnOp]) {
      res = visitUnOp(ast.asInstanceOf[UnOp])
    }
    else if (ast.isInstanceOf[FuncBody]) {
      res = visitFuncBody(ast.asInstanceOf[FuncBody])
    }
    else if (ast.isInstanceOf[FuncDecl]) {
      res = visitFuncdecl(ast.asInstanceOf[FuncDecl])
    }
    else if (ast.isInstanceOf[FuncParam]) {
      res = visitFuncParam(ast.asInstanceOf[FuncParam])
    }
    else if (ast.isInstanceOf[StmtList]) {
      res = visitStmtList(ast.asInstanceOf[StmtList])
    }
    else if (ast.isInstanceOf[FuncCallStmt]) {
      res = visitFuncCallStmt(ast.asInstanceOf[FuncCallStmt])
    }
    else if (ast.isInstanceOf[FuncCallExp]) {
      res = visitFuncCallExp(ast.asInstanceOf[FuncCallExp])
    }
    else if (ast.isInstanceOf[IfStmt]) {
      res = visitIfStmt(ast.asInstanceOf[IfStmt])
    }
    else if (ast.isInstanceOf[WhileStmt])  {

      res = visitWhileStmt(ast.asInstanceOf[WhileStmt])
    }
    else if (ast.isInstanceOf[DoWhileStmt])  {
      res = visitDoWhileStmt(ast.asInstanceOf[DoWhileStmt])
    }
    else if (ast.isInstanceOf[Id]) {
      res = visitId(ast.asInstanceOf[Id])
    }
    else if (ast.isInstanceOf[Intlit]) {
      res = visitIntlit(ast.asInstanceOf[Intlit])
    }
    else if (ast.isInstanceOf[Floatlit]) {
      res = visitFloatlit(ast.asInstanceOf[Floatlit])
    }
    else if (ast.isInstanceOf[Boollit]) {
      res = visitBoollit(ast.asInstanceOf[Boollit])
    }
    else if (ast.isInstanceOf[AssignStmt]) {
      res = visitAssignStmt(ast.asInstanceOf[AssignStmt])
    }
    else if (ast.isInstanceOf[ForStmt]) {
      res = visitForStmt(ast.asInstanceOf[ForStmt])
    }
    else if (ast.isInstanceOf[BreakStmt]) {
      res = visitBreakStmt(ast.asInstanceOf[BreakStmt])
    }
    else if (ast.isInstanceOf[Continue]) {
      res = visitContinue(ast.asInstanceOf[Continue])
    }
    else if (ast.isInstanceOf[ReturnStmt]) {
      res = visitReturnStmt(ast.asInstanceOf[ReturnStmt])
    }
    else if (ast.isInstanceOf[VarDecl]) {
      res = visitVarDecl(ast.asInstanceOf[VarDecl])
    }
    else if (ast.isInstanceOf[IdInit]) {
      res = visitIdInit(ast.asInstanceOf[IdInit])
    }
    else if (ast.isInstanceOf[Arraylit]) {
      res = visitArrayLit(ast.asInstanceOf[Arraylit])
    }
    else if (ast.isInstanceOf[ArrayCell]) {
      res = visitArrayCell(ast.asInstanceOf[ArrayCell])
    }
    res
  }

  def visitProgram(ast: Program): String = {
    var res = "OK"
    for (item <- ast.decl) {
      res = visit(item)
    }
    "OK"
  }
  def visitVarDecl(ast: VarDecl): String = {
    var res = "OK"
    for (item <- ast.varDecl) {

      res = visit(item)

    }
    "OK"
  }
//Checking if the variable is already declared:
  def visitIdInit(ast: IdInit): String = {
    var res = "OK"
    res =  visit(ast.varInit)
    //Checking & thow the Exception if is already declared
    if (this.indexDecl.indexOf(ast.variable.name) > -1) {
      throw new Exception(ast.variable.name + "  is already declared: ")
    }


  //If not, Store the information of variable if it can be declare
    this.detailDecl = (res) :: this.detailDecl
    this.indexDecl = (ast.variable.name) :: this.indexDecl
    res
  }

  //Checking if 2 expressions have the same type & the mismatch of the operands &  operators
  def visitBinOp(ast: BinOp): String = {

    //Visit 2 expression to get the exactly type
    var vse1 = visit(ast.e1)
    var vse2 = visit(ast.e2)

    //Checking if 2 expressions have the same type
    if (vse1 != vse2 ) {

      throw new Exception("Expression 1 and expression 2 must have the same type " + ast.e1 +"[" + vse1 + "]  && " + ast.e2 +"[" + vse2 + "]" )
    }

    //Checking the mismatch of the operands &  operators
    //The operands of the following operators are in boolean type:
    //! && ||
    if ((ast.op == "&&") ||(ast.op == "||") || (ast.op == "!")) {
        if ((vse1 != "Boolit") || (vse2 != "Boolit")) {
          throw new Exception("The operands of operators [" + ast.op + "] should be in boolean type: " + ast)
        }
    }
      //A value of type integer may be positive or negative. Only these operators
    //can act on integer values:
    //+ - * \ \%
    //== != < > <= >=
   else if ((ast.op == "+" )|| (ast.op =="-" )|| (ast.op =="*") || (ast.op =="\\") || (ast.op =="\\%")||
      (ast.op == "==") || (ast.op =="!=") || (ast.op =="<") ||(ast.op == ">") || (ast.op =="<=" )|| (ast.op ==">=")) {
        if ((vse1 != "Intlit") || (vse2 != "Intlit")){
        throw new Exception("The operands of operators ["+ ast.op +"] should be in interger type: "+ ast)
      }
    }
      //The operands of the following operators are in float type:
    //+. -. *. \.
    //=/= <. >. <=. >=.
      else if ((ast.op == "+." )|| (ast.op =="-." )|| (ast.op =="*.") || (ast.op =="\\.") || (ast.op =="=/=")||
        (ast.op =="<.") ||(ast.op == ">.") || (ast.op =="<=." )|| (ast.op ==">=.")) {
        if ((!ast.e1.isInstanceOf[Floatlit]) || (!ast.e2.isInstanceOf[Floatlit])) {
          if ((vse1 != "Floatlit") || (vse2 != "Floatlit"))
          throw new Exception("The operands of operators ["+ ast.op +"] should be in float type: "+ ast)
        }

    }
    //Return type of expression
    if ((ast.op == "==") || (ast.op == "!=") || (ast.op == "<")||(ast.op == ">")||(ast.op == "<=")||(ast.op == ">=")||(ast.op =="=/=")||(ast.op == "<.")||(ast.op == ">.")||(ast.op == "<=.")||(ast.op == ">=."))
    "Boolit"
    else vse1
    }

// Checking the same type of LHS = RHS in Assignment statement
  def visitAssignStmt(ast: AssignStmt): String = {

    //Visit 2 expression to get the exactly type
    var res1 = visit(ast.lhs)
    var res2 = visit(ast.rhs)
   //Compare 2 expression's type
   if (res1 != res2){
     throw new Exception("For an assignment statement RHS[" + res2 +"] should be same type as LSH["+ res1 +"]")
   }
    "OK"
   }


    def visitFuncdecl(ast: FuncDecl): String = {
    var res = visit(ast.param)
    res = visit(ast.name)
    res = visit(ast.param)
    res = visit(ast.body)
      res
  }

  def visitUnOp(ast:UnOp): String = {
    var res = visit(ast.e1)
    res
  }

  def visitFuncParam(ast: FuncParam): String = {

      var res = "OK"
      for (item <- ast.param) {
        res = visit(item)
      }
      res
    }



  def visitFuncBody(ast: FuncBody): String = {
    visit(ast.stmtList)

  }

  def visitStmtList(ast: StmtList): String = {
    var res = "OK"
    for (item <- ast.varDecl) {
      res = visit(item)
    }
    for (item <- ast.stmtList) {
      res = visit(item)
    }
    res
  }

  def visitFuncCallStmt(ast:FuncCallStmt): String =
    {
      var res = visit(ast.method)
    for (item <- ast.para) {
     res = visit(item)
    }
    res
  }

  def visitFuncCallExp(ast:FuncCallExp): String = {
    var res = "NoneType"

    println("Visit name " + ast.name.name)
    var param = "OK"
    for (item <- ast.para) {
        // println("ssss" + item)
        param = visit(item)
      }
//Return the type of expression
    if (ast.name.name == "int_of_float)") {
      res = "Intlit"
    }
    else if (ast.name.name =="float_to_int)") {
      res = "Floatlit"
    }
    else if (ast.name.name == "int_of_string") {
      res = "Intlit"
    }
    else if (ast.name.name == "string_of_int") {
      res = "Stringlit"
    }
    else if (ast.name.name == "float_of_string") {
      res = "Floatlit"
    }
    else if (ast.name.name == "string of float") {
      res = "Stringlit"
    }
    else if (ast.name.name == "bool_of_string") {
      res = "Boolit"
    }
    else if (ast.name.name == "string of bool") {
      res = "Stringlit"
    }
    else res = "void"

    res
  }

  def visitIfStmt(ast:IfStmt): String =
  {
    var res = "OK"

    for ((a,b) <- ast.ifList) {
       res = visit(a)


    if (res != "Boolit" ) {
        throw new Exception("The type of a condition expression in statement must be boolean: " + a)
      }
      res = visit(b)
    }
    res
  }
  def visitWhileStmt(ast:WhileStmt): String =
  {

      var res = visit(ast.exp)
      if (res != "Boolit") {
        throw new Exception("The type of a condition expression in statement must be boolean: "+ast.exp )
      }
   res= visit(ast.stmtList)
    res
    }
  def visitBreakStmt(ast:BreakStmt) : String = {
   "OK"
  }
  def visitContinue(ast:Continue) : String = {
    "OK"
  }

  def visitReturnStmt(ast:ReturnStmt) : String = {
    visit(ast.exp)
  }

  def visitForStmt(ast:ForStmt): String =
  {

   var res = visit(ast.exp1)
    res = visit(ast.exp2)
   if  (res != "Boolit") {

       throw new Exception("ConditionExpr must be of boolean type.: "+ast.exp2  )

     }
    res = visit(ast.exp3)
    res = visit(ast.stmtList)
    res
  }

  def visitDoWhileStmt(ast:DoWhileStmt): String =
  {
    var res= visit(ast.stmtList)
     res = visit(ast.exp)
    if (res != "Boolit") {
      throw new Exception("The type of a condition expression in statement must be boolean "+res  )
    }

    res
  }
  def visitId(ast:Id) : String = {
    println("Visit Ida")
    var res = this.indexDecl.indexOf(ast.name)
    println("index" + res )
    if (res > -1) this.detailDecl(res)
    else
    "Id"
  }

  def visitIntlit(ast:Intlit) : String = {
    println("Visit Intlit")
    "Intlit"
  }

  def visitFloatlit(ast:Floatlit) : String = {
    println("Visit Floatlit")
    "Floatlit"
  }

  def visitBoollit(ast:Boollit) : String = {
    println("Visit Boollit")
    "Boollit"
  }
  def visitStringlit(ast:Stringlit) : String = {
    println("Visit Stringlit")
    "Stringlit"
  }
//Checking the element's type in an array
  def visitArrayLit(ast:Arraylit) : String = {
    println("Visit Arraylit")
    var res = "Arraylit"
    if (ast.lit.size >0) {
      var visitLit = visit(ast.lit(0))
      for (item <- ast.lit) {
        if (visit(item) != visitLit){
          throw new Exception("All element of array should have the same type: "+ visit(item) +" <> " +visitLit )
        }
      }
      //Return the size & type of array
      res = res + " " + visitLit + " " + ast.lit.size
    }
    res
  }
  //Checking the element's type in an array
  def visitArrayCell(ast:ArrayCell) : String = {
    var res = "ArrayCell["
    if (ast.idx.size >0) {
      var visitLit = visit(ast.idx(0))
      for (item <- ast.idx) {
        if (visit(item) != visitLit){
          throw new Exception("All element of array should have the same type: "+ visit(item) +" <> " +visitLit )
        }
      }
      res = res + ast.idx.size +"]"
    }
    res
  }

}

object ASTGen extends App {
  try {
    val source = new ANTLRFileStream("test.txt")
    val lexer = new BKITLexer(source)
    val tokens = new CommonTokenStream(lexer)
    val parser = new BKITParser(tokens)
    val bkit_parsetree = parser.program()
    val astgen = new ASTGen()
    val bkit_ast = bkit_parsetree.accept(astgen)
    val static_check = new StaticCheck()
   val st_ast = static_check.visit(bkit_ast)
    println("Static Check = " + st_ast)
    println(bkit_ast)
  }
  catch {
    case ex: Exception => println("Type Mismatch In Statement: " + ex)
  }
}
/*10:06 AM
Qui Trình:
antlr4  BKIT.g4
antlr4 -visitor BKIT.g4
javac BKIT*.java

grun BKIT program -tokens

scalac ASTGen.scala --> sinh ra file .class
scala ASTGen --> Chạy chương trình*/
