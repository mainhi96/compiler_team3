
object Main extends App {
  def main(args: Array[String]): Unit = {
    val source = new ANTLRFileStream("test.txt")
    val lexer = new BKITLexer(source)
    val tokens = new CommonTokenStream(lexer)
    val parser = new BKITParser(tokens)
    val bkit_parsetree = parser.program()
    val astgen = new ASTGen()
    val bkit_ast = bkit_parsetree.accept(astgen)
    println(bkit_ast)
  }
}